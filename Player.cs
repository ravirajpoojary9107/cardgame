using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGame1
{
    public  class Player
    {
        public string Name { get; set; }
        public Queue<Card> Deck { get; set; }
        public Queue<Card> DiscardPile { get; set; }

        public Player(string name)
        {
            this.Name = name;
            CreateDeckAndDiscard();

        }

        private void CreateDeckAndDiscard()
        {
            this.Deck = new Queue<Card>();
            this.DiscardPile = new Queue<Card>();
        }
    }
}
